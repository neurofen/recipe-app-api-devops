#!/bin/sh

set -e ## if errer raised don't continue with other commands

python manage.py collectstatic --noinput 
python manage.py wait_for_db ## wait for database to be available
python manage.py migrate ## migrate chanes to database

## Run uwsgi service on port 9000 as the master service  with 4 workers in our container.
## Enable multi-threading.
## Server app as the uwsgi service 
uwsgi --socket :9000 --workers 4 --master --enable-threads --module app.wsgi
