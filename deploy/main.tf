terraform {
  backend "s3" {
    bucket         = "recipe-app-api-devops-jpott-tfstate"
    key            = "recipe-app.tfstate"
    region         = "ap-southeast-2"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region  = var.region
  version = "~> 2.54.0"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "terraform"
  }
}