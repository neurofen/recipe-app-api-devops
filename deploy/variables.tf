variable "region" {
  description = "This is the cloud hosting region"
  type        = string
}

variable "sg_ssh" {
  description = "This is the public SSH security group"
  type        = string
}

variable "default_vpc" {
  description = "This is the default vpc for the account"
  type        = string
  default     = "vpc-746c5013"
}

variable "prefix" {
  description = "Used to prefix all of the names of our resources"
  type        = string
  default     = "raad"
}

variable "project" {
  type    = string
  default = "recipe-app-api-devops"
}

variable "contact" {
  type    = string
  default = "jpott@bitskilz.com.au"
}