data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

module "security_group_ssh" {
  source              = "terraform-aws-modules/security-group/aws//modules/ssh"
  version             = "3.10.0"
  name                = var.sg_ssh
  description         = "Security group for SSH open within VPC"
  vpc_id              = var.default_vpc
  ingress_cidr_blocks = ["0.0.0.0/0"]
}

resource "aws_subnet" "main" {
  vpc_id                  = var.default_vpc
  cidr_block              = "172.31.1.0/24"
  map_public_ip_on_launch = true
  tags = {
    Name = "Main"
  }
}

resource "aws_instance" "bastion" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = "t2.micro"
  key_name               = "acloudguru"
  vpc_security_group_ids = [module.security_group_ssh.this_security_group_id]
  subnet_id              = aws_subnet.main.id

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}
